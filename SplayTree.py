class Node:
    def __init__(self, value):
        self.left = None
        self.right = None
        self.parent = None
        self.value = value
        self._checkRep()

    def hasRightChild(self):
        return self.right is not None

    def hasLeftChild(self):
        return self.left is not None

    def setLeftChild(self, leftNode):
        self.left = leftNode
        if (self.left):
            self.left.parent = self
        self._checkRep()

    def setRightChild(self, rightNode):
        self.right = rightNode
        if (self.right):
            self.right.parent = self
        self._checkRep()

    def isLeaf(self):
        return self.left is None and self.right is None

    def isRoot(self):
        return self.parent is None

    def isLeftChild(self):
        return self.parent.left == self

    def isRightChild(self):
        return self.parent.right == self

    def hasOnlyOneChild(self):
        return (self.left is not None and self.right is None) or (self.left is None and self.right is not None)

    def getOnlyChild(self):
        if self.right is not None:
            return self.right
        else:
            return  self.left

    def hasBothChildren(self):
        return self.right is not None and self.left is not None

    def changeChild(self, fromChild, toChild):
        if self.left and self.left.value == fromChild.value:
            self.left = toChild
            if toChild is not None:
                toChild.parent = self.left
        elif self.right and self.right.value == fromChild.value:
            self.right = toChild
            if toChild is not None:
                toChild.parent = self.right
        self._checkRep()

    def __str__(self):
        leftValue = ""
        rightValue = ""
        if (self.left):
            leftValue = self.left.value
        if (self.right):
            rightValue = self.right.value
        return "({}): left: ({}) right({})".format(self.value, leftValue, rightValue)


    def _checkRep(self):
        isValid = True
        if self.left:
            isValid = self.left.value < self.value
        if self.right:
            isValid = self.right.value > self.value

        if not isValid:
            print "Node checkRep violation: {}".format(str(self))

        assert(isValid)




class SplayTree:

    def __init__(self):
        self.root = None
        self.size = 0

    # Insere um valor na avore e nao retorna nenhum valor
    def insert(self, value):
        if self.root:
            self._insert(value, self.root)
        else:
            self.root = Node(value)
            self.size += 1

        self._checkRep()

    def _insert(self, value, currentNode):
        if value == currentNode.value:
            # Ignora valores duplicados
            return
        elif value < currentNode.value:
            if currentNode.hasLeftChild():
                self._insert(value, currentNode.left)
            else:
                currentNode.setLeftChild(Node(value))
                self.size += 1
        else:
            if currentNode.hasRightChild():
                self._insert(value, currentNode.right)
            else:
                currentNode.setRightChild(Node(value))
                self.size += 1

        self._checkRep()


    # Remove um no da arvore caso o encontre e retorna true, false caso contrario
    def delete(self, value):
        node = self._find(value)
        if not node:
            return False

        # caso 1: eh uma folha
        if node.isLeaf():
            if not node.isRoot():
                node.parent.changeChild(node, None)
            else:
                self.root = None
        # caso 2: possui apenas um filho
        elif node.hasOnlyOneChild():
            child = node.getOnlyChild()
            if not node.isRoot():
                node.parent.changeChild(node, child)
            else:
                self.root = child
                child.parent = None
        # caso 3: possui dois filhos
        else:
            mostLeftLeaf = self._findMostLeftLeaf(node.right)

            leftChild = node.left
            rightChild = node.right

            if rightChild != mostLeftLeaf:
                mostLeftLeaf.parent.setLeftChild( mostLeftLeaf.right)

            mostLeftLeaf.right = None

            if node.isRoot():
                self.root = mostLeftLeaf
                mostLeftLeaf.parent = None
            else:
                node.parent.setRightChild(mostLeftLeaf)

            mostLeftLeaf.setLeftChild(leftChild)
            if rightChild != mostLeftLeaf:
                mostLeftLeaf.setRightChild(rightChild)

        self.size -= 1
        self._checkRep()
        return True

    # Retorna um verdadeiro caso o valor tenha sendo encontrado
    def find(self, value):
        return self._find(value) is not None

    def _find(self, value):
        currentNode = self.root
        while currentNode:
            if currentNode.value == value:
                return currentNode
            elif value < currentNode.value:
                currentNode = currentNode.left
            else:
                currentNode = currentNode.right
        return None

    def _findMostLeftLeaf(self, currentNode):
        while currentNode.hasLeftChild():
            currentNode = currentNode.left
        return currentNode

    def getSize(self):
        return self.size


    def _splay(self, node):
        while not node.isRoot():
            if node.parent.isRoot():
                self._zig(node)
            elif (node.isRightChild() and node.parent.isRightChild()) or (node.isLeftChild() and node.parent.isLeftChild()):
                self._zigZig(node)
            else:
                self._zigZag(node)


    # So pode ser chamado quando p eh root
    def _zig(self, x):
        p = x.parent
        b = x.right
        a = x.left
        assert(p.isRoot())


        if x.isLeftChild():
            p.setLeftChild(b)
            x.setRightChild(p)
        else:
            p.setRightChild(a)
            x.setLeftChild(p)


        self.root = x
        x.parent = None

        self._checkRep()


    def _zigZig(self, x):
        p = x.parent
        g = p.parent
        a = x.left
        b = x.right

        gg = None
        isGRoot = g.isRoot()
        isGRightChild = True
        if (not isGRoot):
            gg = g.parent
            if g.isLeftChild():
                isGRightChild = False

        if x.isLeftChild():
            c = p.right
            d = g.right
            p.setLeftChild(b)
            g.setLeftChild(c)
            x.setRightChild(p)
            p.setRightChild(g)
        else:
            c = p.left
            d = g.left
            p.setRightChild(a)
            g.setRightChild(c)
            x.setLeftChild(p)
            p.setLeftChild(g)

        if isGRoot:
            self.root = x
            x.parent = None
        else:
            if isGRightChild:
                gg.setRightChild(x)
            else:
                gg.setLeftChild(x)
        self._checkRep()




    def _zigZag(self, x):
        p = x.parent
        g = p.parent

        gg = None
        isGRoot = g.isRoot()
        isGRightChild = True
        if (not isGRoot):
            gg = g.parent
            if g.isLeftChild():
                isGRightChild = False

        if x.isRightChild():
            a = p.left
            b = x.left
            c = x.right
            d = g.right

            p.setRightChild(b)
            g.setLeftChild(c)
            x.setLeftChild(p)
            x.setRightChild(g)

        else:
            a = p.right
            b = x.right
            c = x.left
            d = g.left

            p.setLeftChild(b)
            g.setRightChild(c)
            x.setLeftChild(g)
            x.setRightChild(p)


        if isGRoot:
            self.root = x
            x.parent = None
        else:
            if isGRightChild:
                gg.setRightChild(x)
            else:
                gg.setLeftChild(x)
        self._checkRep()

    def _checkRep(self):
        nodes = []
        if (self.root):
            nodes.append(self.root)

        while len(nodes)>0:
            currentNode = nodes.pop()
            currentNode._checkRep()
            if currentNode.hasLeftChild():
                nodes.append(currentNode.left)
            if currentNode.hasRightChild():
                nodes.append(currentNode.right)
