__author__ = 'rodrigo'

from unittest import *
from SplayTree import *
class SplayTreeTest(TestCase):

    def testNodeInit(self):
        myNode = Node(4)
        self.assertEquals(4, myNode.value)
        self.assertIsNone(myNode.left)
        self.assertIsNone(myNode.right)

    def testStrNode(self):
        myNode = Node(10)
        self.assertEquals("(10): left: () right()", str(myNode))
        myNode.setRightChild(Node(12))
        self.assertEquals("(10): left: () right(12)", str(myNode))
        myNode.setLeftChild(Node(8))
        self.assertEquals("(10): left: (8) right(12)", str(myNode))

    def testSimpleInsert(self):
        tree = SplayTree()
        tree.insert(5)
        self.assertEquals(1, tree.getSize())
        tree.insert(3)
        self.assertEquals(2, tree.getSize())
        tree.insert(2)
        self.assertEquals(3, tree.getSize())

    def testSimpleFind(self):
        tree = SplayTree()
        tree.insert(5)
        tree.insert(2)
        tree.insert(8)
        tree.insert(9)

        self.assertTrue(4, tree.getSize())

        self.assertTrue( tree.find(5))
        self.assertTrue( tree.find(2))
        self.assertTrue( tree.find(8))
        self.assertTrue( tree.find(9))
        self.assertFalse( tree.find(0) )
        self.assertFalse( tree.find(11) )
        self.assertFalse( tree.find(7) )

    def testDeleteRoot(self):
        tree = SplayTree()
        tree.insert(5)
        self.assertTrue(tree.delete(5))
        self.assertEquals(0, tree.getSize())

        tree.insert(8)
        tree.insert(9)
        self.assertTrue(tree.delete(8))
        self.assertEquals(1, tree.getSize())
        self.assertTrue(tree.delete(9))

        self.assertEquals(0, tree.getSize())
        tree.insert(8)
        self.assertEquals(1, tree.getSize())
        tree.insert(5)
        self.assertEquals(2, tree.getSize())
        tree.insert(10)
        self.assertEquals(3, tree.getSize())
        self.assertTrue(tree.delete(8))
        self.assertEquals(2, tree.getSize())

    def testDelete(self):
        tree = SplayTree()
        tree.insert(5)
        tree.insert(2)
        tree.insert(8)
        tree.insert(1)
        tree.insert(3)
        #     5
        #    / \
        #   2   8
        #  / \
        # 1   3
        self.assertEquals(5, tree.getSize())

        # Apagando uma folha
        self.assertTrue(tree.delete(1))
        #     5
        #    / \
        #   2   8
        #    \
        #     3
        self.assertTrue(tree.find(5))
        self.assertTrue(tree.find(2))
        self.assertTrue(tree.find(8))
        self.assertTrue(tree.find(3))
        self.assertFalse(tree.find(1))

        # Apagando com um filho
        self.assertTrue(tree.delete(2))
        #     5
        #    / \
        #   3   8
        #
        #
        self.assertTrue(tree.find(5))
        self.assertTrue(tree.find(8))
        self.assertTrue(tree.find(3))
        self.assertFalse(tree.find(2))
        self.assertFalse(tree.find(1))
        self.assertEquals("(5): left: (3) right(8)", str(tree.root))
        self.assertEquals("(3): left: () right()", str(tree.root.left))
        self.assertEquals("(8): left: () right()", str(tree.root.right))

        # Apagando com dois filhos .. primeiro criar os filhos
        tree.insert(7)
        tree.insert(10)
        #     5
        #    / \
        #   3   8
        #      / \
        #     7   10
        self.assertEquals("(8): left: (7) right(10)", str(tree.root.right))

        self.assertTrue(tree.delete(8))
        #     5
        #    / \
        #   3   10
        #      /
        #     7
        self.assertEquals("(5): left: (3) right(10)", str(tree.root))
        self.assertEquals("(10): left: (7) right()", str(tree.root.right))
        self.assertTrue(tree.find(5))
        self.assertTrue(tree.find(3))
        self.assertTrue(tree.find(7))
        self.assertTrue(tree.find(10))
        self.assertFalse(tree.find(2))
        self.assertFalse(tree.find(1))
        self.assertFalse(tree.find(8))

        self.assertTrue(tree.delete(5))
        #     7
        #    / \
        #   3   10
        #
        #
        self.assertEquals("(7): left: (3) right(10)", str(tree.root))
        self.assertFalse(tree.find(5))
        self.assertTrue(tree.find(7))
        self.assertTrue(tree.find(3))
        self.assertTrue(tree.find(10))

        self.assertTrue(tree.delete(7))
        #     10
        #    /
        #   3
        #
        #
        self.assertEquals("(10): left: (3) right()", str(tree.root))
        self.assertFalse(tree.find(7))
        self.assertTrue(tree.find(3))
        self.assertTrue(tree.find(10))


        self.assertTrue(tree.delete(10))
        #     3
        self.assertEquals("(3): left: () right()", str(tree.root))
        self.assertFalse(tree.find(10))
        self.assertTrue(tree.find(3))

        self.assertTrue(tree.delete(3))
        #
        self.assertIsNone(tree.root)
        self.assertFalse(tree.find(3))
        self.assertEquals(0, tree.getSize())


    def testZigLeft(self):
        tree = SplayTree()
        tree.insert(10)
        tree.insert(5)
        tree.insert(15)
        tree.insert(12)
        tree.insert(17)
        self.assertEquals("(10): left: (5) right(15)", str(tree.root))
        self.assertEquals("(15): left: (12) right(17)", str(tree.root.right))

        node = tree._find(15)
        tree._zig(node)
        self.assertEquals("(15): left: (10) right(17)", str(tree.root))
        self.assertEquals("(10): left: (5) right(12)", str(tree.root.left))


    def testZigRight(self):
        tree = SplayTree()
        tree.insert(10)
        tree.insert(5)
        tree.insert(2)
        tree.insert(7)
        tree.insert(15)
        self.assertEquals("(10): left: (5) right(15)", str(tree.root))
        self.assertEquals("(5): left: (2) right(7)", str(tree.root.left))

        node = tree._find(5)
        tree._zig(node)
        self.assertEquals("(5): left: (2) right(10)", str(tree.root))
        self.assertEquals("(10): left: (7) right(15)", str(tree.root.right))

    def testZigZigLeft(self):
        tree = SplayTree()
        tree.insert(20)
        tree.insert(25)
        tree.insert(10)
        tree.insert(15)
        tree.insert(5)
        tree.insert(7)
        tree.insert(1)
        #      20
        #      / \
        #    10   25
        #    / \
        #   5  15
        #  / \
        # 1   7

        node = tree._find(5)
        tree._zigZig(node)
        self.assertEquals("(5): left: (1) right(10)", str(tree.root))
        self.assertEquals("(1): left: () right()", str(tree.root.left))
        self.assertEquals("(10): left: (7) right(20)", str(tree.root.right))
        self.assertEquals("(20): left: (15) right(25)", str(tree.root.right.right))


    def testZigZigRight(self):
        tree = SplayTree()
        tree.insert(20)
        tree.insert(15)
        tree.insert(30)
        tree.insert(25)
        tree.insert(40)
        tree.insert(35)
        tree.insert(45)
        #      20
        #      / \
        #    15   30
        #        / \
        #      25   40
        #          / \
        #        35   45
        node = tree._find(40)
        tree._zigZig(node)
        #        40
        #        / \
        #      30   45
        #     / \
        #   20   35
        #  / \
        # 15 25

        self.assertEquals("(40): left: (30) right(45)", str(tree.root))
        self.assertEquals("(45): left: () right()", str(tree.root.right))
        self.assertEquals("(30): left: (20) right(35)", str(tree.root.left))
        self.assertEquals("(20): left: (15) right(25)", str(tree.root.left.left))


    def testZigZigRightNoRoot(self):
        tree = SplayTree()
        tree.insert(17)
        tree.insert(15)
        tree.insert(20)
        tree.insert(18)
        tree.insert(30)
        tree.insert(25)
        tree.insert(40)
        tree.insert(35)
        tree.insert(45)
        #   17
        #  /  \
        # 15   20
        #     / \
        #   18   30
        #       / \
        #     25   40
        #         / \
        #       35   45
        node = tree._find(40)
        tree._zigZig(node)
        #      17
        #     /  \
        #    15  40
        #        / \
        #      30   45
        #     / \
        #   20   35
        #  / \
        # 18 25

        self.assertEquals("(17): left: (15) right(40)", str(tree.root))
        self.assertEquals("(15): left: () right()", str(tree.root.left))
        self.assertEquals("(40): left: (30) right(45)", str(tree.root.right))
        self.assertEquals("(30): left: (20) right(35)", str(tree.root.right.left))
        self.assertEquals("(20): left: (18) right(25)", str(tree.root.right.left.left))

    def testZigZagRight(self):
        tree = SplayTree()
        tree.insert(10)
        tree.insert(5)
        tree.insert(30)
        tree.insert(20)
        tree.insert(35)
        tree.insert(15)
        tree.insert(25)
        #   10
        #   / \
        #  5  30
        #     / \
        #   20   35
        #   /\
        # 15 25
        node = tree._find(20)
        tree._zigZag(node)
        self.assertEquals("(20): left: (10) right(30)", str(tree.root))
        self.assertEquals("(10): left: (5) right(15)", str(tree.root.left))
        self.assertEquals("(30): left: (25) right(35)", str(tree.root.right))

    def testZigZagLeft(self):
        tree = SplayTree()
        tree.insert(40)
        tree.insert(20)
        tree.insert(45)
        tree.insert(10)
        tree.insert(30)
        tree.insert(25)
        tree.insert(35)
        #      40
        #     / \
        #   20  45
        #   / \
        # 10  30
        #     / \
        #   25   35
        node = tree._find(30)
        tree._zigZag(node)
        self.assertEquals("(30): left: (20) right(40)", str(tree.root))
        self.assertEquals("(20): left: (10) right(25)", str(tree.root.left))
        self.assertEquals("(40): left: (35) right(45)", str(tree.root.right))


    def testZigZagRightNoRoot(self):
        tree = SplayTree()
        tree.insert(8)
        tree.insert(10)
        tree.insert(5)
        tree.insert(9)
        tree.insert(30)
        tree.insert(20)
        tree.insert(35)
        tree.insert(15)
        tree.insert(25)
        #   8
        #  / \
        # 5  10
        #    / \
        #  9  30
        #     / \
        #   20  35
        #   /\
        # 15 25
        node = tree._find(20)
        tree._zigZag(node)
        #   8
        #  / \
        # 5   20
        #     / \
        #   10   30
        #  / \   / \
        # 9  15 25 35

        self.assertEquals("(8): left: (5) right(20)", str(tree.root))
        self.assertEquals("(20): left: (10) right(30)", str(tree.root.right))
        self.assertEquals("(10): left: (9) right(15)", str(tree.root.right.left))
        self.assertEquals("(30): left: (25) right(35)", str(tree.root.right.right))

    def testSplay(self):
        tree = SplayTree()
        tree.insert(8)
        tree.insert(10)
        tree.insert(5)
        tree.insert(9)
        tree.insert(30)
        tree.insert(20)
        tree.insert(35)
        tree.insert(15)
        tree.insert(25)
        #   8
        #  / \
        # 5  10
        #    / \
        #  9  30
        #     / \
        #   20  35
        #   /\
        # 15 25
        node = tree._find(25)
        tree._splay(node)
        self.assertEquals("(25): left: (10) right(30)", str(tree.root))
        self.assertEquals("(10): left: (8) right(20)", str(tree.root.left))
        self.assertEquals("(30): left: () right(35)", str(tree.root.right))
        self.assertEquals("(8): left: (5) right(9)", str(tree.root.left.left))
        self.assertEquals("(20): left: (15) right()", str(tree.root.left.right))
        self.assertEquals("(35): left: () right()", str(tree.root.right.right))





















